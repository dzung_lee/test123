# Overview

The goal of this repository is to provide a *high quality, professional* example for setting up a Python Based Continuous Integration pipeline using [GitLab Continous Integration Pipelines](https://about.gitlab.com/product/continuous-integration/).

**Note:** *This example is currently in **DRAFT** form and may not be fully tested.  If you should find errors or encounter issues please [let us know](mailto:support@cloudrepo.io).


# Python Example

We provide an *extremely* basic python example to serve as the codebase for this example. 

Below is an example of running it at a console:

```
$ python example/hello
Hello, my friend, you're running an extremely basic python example!
$
```


## Bundling Example Up (Create Dist Package)

**Note:** Make sure you've run `pip install setuptools twine` if you're going to attempt to follow along and deploy this example outside of GitLab CI.

In line with the simplicity of our example, we package up our dist package with the following commands:

`python setup.py sdist`

This will create a `dist` folder and a file named `cloudrepo-gitlab-ci-example-1.0.0.tar.gz`.   This example will show how to push this to a PyPi compatible repository, in particular [CloudRepo](https://www.cloudrepo.io).

Okay, that's enough of the Python specific stuff, let's move on to the GitLab CI.

# Getting Started

This example is based off the information found in GitLab's [Getting Started with GitLab CI/CD - Quick Start](https://gitlab.com/help/ci/quick_start/README).  If you have additional questions or want to learn more, that's a good place to start.

GitLab CI is enabled by default on all projects.   All you have to do is add a 
`.gitlab-ci.yml` file to the root directory of your project.  

Once this is done, all pushes to your GitLab repository will start the pipeline.


## Disable Public Pipelines

If your pipelines are public then anyone can see the output logs and artifacts.  Allowing public access to your logs could increase the risk of an accidental leak of sensitive information that is logged during your build.

It is our recommendation that you *disable Public Pipelines* for any pipeline that you are using to push to CloudRepo.   This setting can be found in the Git Lab Project Settings under the **CI/CD -> General Pipeline Settings** path.

Read more about the [Visibility of Pipelines](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#visibility-of-pipelines).

For our example, we have ensured no accidental logging of credentials and have made the pipeline public so that you can view the output of our jobs to compare against yours should you have any problems.

## Adding Environment Variables

You don't want to store credentials in your source code repository, so GitLab provides **Environment Variables** that allow you to securely store your CloudRepo username and password.

If you want to learn more, check out [GitLab Environment Variables](https://gitlab.com/help/ci/variables/README#variables).  

For the purposes of this example we want to set the following three environment variables:

**Note:** All of these will end up in the generated `.pypirc` file, so make sure you don't that to version control.

* CLOUDREPO_USERNAME
  * This is the username for pushing and pulling artifacts from your private pip repository.  
* CLOUDREPO_PASSPHRASE
  * This is the user's passphrase for pushing and pulling artifacts from your private pip repository.  
* CLOUDREPO_ORGANIZATION
  * This is the CloudRepo organization that's associated with the user you specified here.  
* CLOUDREPO_REPOSITORY
  * This is the CloudRepo repository that you wish to push packages.  

  ![GitLab CI Environment Variables](images/environment-variables.png)

## .gitlab-ci.yml

For a deeper guide to the `.gitlab-ci.yml`, please see [What is .gitlab-ci.yml?](https://gitlab.com/help/ci/quick_start/README#what-is-gitlab-ciyml) in the documentation.

Our version is configured to install necessary dependencies (python, pip, etc.) and then create a `.pypirc` file which points to your CloudRepo repository (as configured in the Environment Variables, above).

# Additional Validation

You can checkout the logs of our Pipelines and fork this repo for yourself (the build should pick up but you'll need to set your environment variables).

If you have any questions or would like us to elaborate more on anything please [let us know.](mailto:support@cloudrepo.io).
