#!/bin/bash

#
# This file will create a new settings.xml file in this directory containing the variables set for a CloudRepo username and password.
#
# If you don't have a CloudRepo account, create a free trial at https://www.cloudrepo.io
#

#
# Variables to ensure that we read and write out files to the source directory even if we execute from outside.
#
DIR="$(dirname "${BASH_SOURCE[0]}")"
TEMPLATE_FILE=$DIR/pypirc.template
DEST_FILE_NAME=.pypirc
DESTINATION_FILE=$DIR/$DEST_FILE_NAME
GITIGNORE=$DIR/.gitignore

echo "Checking for presence of CLOUDREPO_USERNAME environment variable...."
if [[ $CLOUDREPO_USERNAME ]];
then
  echo "Success - Found CloudRepo Username in environment variables...";
else
  >&2 echo "Failure - CLOUDREPO_USERNAME environment variable not set.  Please set this environment variable and try again."
  exit 1
fi;

echo "Checking for presence of CLOUDREPO_PASSPHRASE environment variable...."
if [[ $CLOUDREPO_PASSPHRASE ]];
then
  echo "Success - Found CloudRepo Passphrase in environment variables...";
else
  >&2 echo "Failure - CLOUDREPO_PASSPHRASE environment variable not set.  Please set this environment variable and try again."
  exit 2
fi;

echo "Checking for presence of CLOUDREPO_ORGANIZATION environment variable...."
if [[ $CLOUDREPO_ORGANIZATION ]];
then
  echo "Success - Found CloudRepo Organization in environment variables...";
else
  >&2 echo "Failure - CLOUDREPO_ORGANIZATION environment variable not set.  Please set this environment variable and try again."
  exit 2
fi;

echo "Checking for presence of CLOUDREPO_REPOSITORY environment variable...."
if [[ $CLOUDREPO_REPOSITORY ]];
then
  echo "Success - Found CloudRepo Repository in environment variables...";
else
  >&2 echo "Failure - CLOUDREPO_REPOSITORY environment variable not set.  Please set this environment variable and try again."
  exit 2
fi;

echo "Environment Variables Set, Creating $DESTINATION_FILE file..."

if [ -f $TEMPLATE_FILE ]; then
   if cat $TEMPLATE_FILE | sed  "s/%CLOUDREPO_ORGANIZATION%/$CLOUDREPO_ORGANIZATION/g" | sed  "s/%CLOUDREPO_REPOSITORY%/$CLOUDREPO_REPOSITORY/g" | sed  "s/%CLOUDREPO_USERNAME%/$CLOUDREPO_USERNAME/g" | sed "s/%CLOUDREPO_PASSPHRASE%/$CLOUDREPO_PASSPHRASE/g" | sed "s/generation text/ATTENTION: This file was generated by the create-settings.sh script.  Do not commit it to version control as it contains credentials!/" > $DESTINATION_FILE; then

      #
      # Let's warn users if they don't have the generated settings.xml file in their .gitignore file.
      #
      # If you don't put the settings.xml file in .gitignore, the maven release plugin will fail during the `mvn release:prepare` step.
      #
      if grep -q "^$DEST_FILE_NAME$" $GITIGNORE; then
         echo "Success - $DEST_FILE_NAME found in $GITIGNORE file.  This will prevent git from saving the credentials in version control."
         else
         echo "Warning - $DEST_FILE_NAME not found in $GITIGNORE file.  Add $DEST_FILE_NAME to $GITIGNORE to ensure you don't accidently commit your credentials!"
      fi

      #
      # Success, we're done here.
      #
      echo "Success - $DESTINATION_FILE file successfully created.  Make sure you do not commit this file to source control."
      exit 0;
   else
      >&2 echo "Failure - There was an error creating the $DESTINATION_FILE";
      exit 3;

   fi;
else
   >&2 echo "Failure - $TEMPLATE_FILE does not exist";
   exit 4;
fi;
